package com.blank.motionlayoutex.transisi

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.blank.motionlayoutex.R

class MainTransisition2 : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main_transisition2)
        overridePendingTransition(R.anim.enter_activity, R.anim.exit_activity)
    }
}