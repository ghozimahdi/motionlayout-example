package com.blank.motionlayoutex.transisi

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.blank.motionlayoutex.R
import kotlinx.android.synthetic.main.activity_main_transisition.*

class MainTransisition : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main_transisition)

        button.setOnClickListener {
            startActivity(Intent(this, MainTransisition2::class.java))
        }
    }
}