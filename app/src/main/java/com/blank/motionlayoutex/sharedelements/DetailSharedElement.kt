package com.blank.motionlayoutex.sharedelements

import android.os.Bundle
import android.transition.Transition
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import com.blank.motionlayoutex.R
import kotlinx.android.synthetic.main.activity_detail_shared_element.*

class DetailSharedElement : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail_shared_element)

        window?.sharedElementEnterTransition?.addListener(
            object : Transition.TransitionListener {
                override fun onTransitionEnd(transition: Transition?) {
                    Log.d("Transition", "End")
                    title = "Detail"
                    tvTitle.animate().rotation(1f)
                }

                override fun onTransitionResume(transition: Transition?) {
                    Log.d("Transition", "Resume")
                }

                override fun onTransitionPause(transition: Transition?) {
                    Log.d("Transition", "Pause")
                }

                override fun onTransitionCancel(transition: Transition?) {
                    Log.d("Transition", "Cancel")
                }

                override fun onTransitionStart(transition: Transition?) {
                    Log.d("Transition", "Start")
                    title = ""
                    tvTitle.animate().rotation(0f)
                }
            })
    }
}