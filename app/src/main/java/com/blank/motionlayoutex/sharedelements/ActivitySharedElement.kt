package com.blank.motionlayoutex.sharedelements

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityOptionsCompat
import androidx.core.util.Pair
import androidx.core.util.Pair.create
import androidx.core.view.ViewCompat
import com.blank.motionlayoutex.R
import kotlinx.android.synthetic.main.activity_shared_element.*


class ActivitySharedElement : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_shared_element)

        goToDetail.setOnClickListener {
            val imageViewTransitionPair: Pair<View, String?> =
                create(imageView, ViewCompat.getTransitionName(imageView))

            val buttonTransitionPair: Pair<View, String> =
                create(goToDetail, ViewCompat.getTransitionName(goToDetail))

            val options = ActivityOptionsCompat.makeSceneTransitionAnimation(
                this,
                imageViewTransitionPair,
                buttonTransitionPair
            )

            startActivity(Intent(this, DetailSharedElement::class.java), options.toBundle())
        }
    }
}