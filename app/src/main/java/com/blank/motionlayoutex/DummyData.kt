package com.blank.motionlayoutex

fun getDummy(): MutableList<String> = mutableListOf(
    "Ghozi",
    "Mahdi",
    "Vira",
    "Marniez",
    "Kalpin",
    "Aji",
    "Veda",
    "Yulius",
    "Depi",
    "Sarah"
)