package com.blank.motionlayoutex.transitionmanager

import android.os.Bundle
import android.view.animation.AnticipateOvershootInterpolator
import androidx.appcompat.app.AppCompatActivity
import androidx.constraintlayout.widget.ConstraintSet
import androidx.transition.ChangeBounds
import androidx.transition.TransitionManager
import com.blank.motionlayoutex.R
import kotlinx.android.synthetic.main.activity_transition_manager.*

class TransitionManagerActivity : AppCompatActivity() {
    var animated = false

    private val constraintSet = ConstraintSet()
    private val constraintReset = ConstraintSet()
    private val transition = ChangeBounds()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_transition_manager)

        constraintSet.clone(rootConstrain)
        constraintReset.clone(rootConstrain)

        transition.interpolator = AnticipateOvershootInterpolator(1.0f)
        transition.duration = 1000

        animateButton.setOnClickListener {
            animated = !animated

            TransitionManager.beginDelayedTransition(rootConstrain, transition)
            constraintSet.connect(
                R.id.logoImage,
                ConstraintSet.BOTTOM,
                R.id.guideline,
                ConstraintSet.TOP
            )

            if (!animated) {
                constraintReset.applyTo(rootConstrain)
            } else {
                constraintSet.applyTo(rootConstrain)
            }
        }
    }
}