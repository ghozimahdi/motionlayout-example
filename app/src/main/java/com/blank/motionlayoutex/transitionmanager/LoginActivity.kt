package com.blank.motionlayoutex.transitionmanager

import android.os.Bundle
import android.view.animation.AnticipateOvershootInterpolator
import androidx.appcompat.app.AppCompatActivity
import androidx.constraintlayout.widget.ConstraintSet
import androidx.transition.ChangeBounds
import androidx.transition.Transition
import androidx.transition.TransitionManager
import com.blank.motionlayoutex.R
import kotlinx.android.synthetic.main.activity_login.*

class LoginActivity : AppCompatActivity() {
    var animated = false

    private val constraintSet = ConstraintSet()
    private val constraintReset = ConstraintSet()
    private val transition = ChangeBounds()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        constraintSet.clone(rootConstraint)
        constraintReset.clone(rootConstraint)

        initializeTransition()

        animateButton.setOnClickListener {
            animated = !animated

            TransitionManager.beginDelayedTransition(rootConstraint, transition)
            constraintSet.connect(
                R.id.logoImage,
                ConstraintSet.BOTTOM,
                R.id.guideline,
                ConstraintSet.TOP
            )
            constraintSet.connect(
                R.id.bottomBox,
                ConstraintSet.TOP,
                R.id.guideline,
                ConstraintSet.TOP
            )

            if (!animated) {
                constraintReset.applyTo(rootConstraint)
            } else {
                constraintSet.applyTo(rootConstraint)
            }
        }
    }

    private fun initializeTransition() {
        transition.apply {
            interpolator = AnticipateOvershootInterpolator(1.0f)
            duration = 1000
            addListener(object : Transition.TransitionListener {
                override fun onTransitionEnd(transition: Transition) {
                    if (animated) {
                        companyName.animate().alpha(1f)
                        email.animate().alpha(1f)
                        password.animate().alpha(1f)
                    }
                }

                override fun onTransitionResume(transition: Transition) {}
                override fun onTransitionPause(transition: Transition) {}
                override fun onTransitionCancel(transition: Transition) {}
                override fun onTransitionStart(transition: Transition) {
                    if (!animated) {
                        companyName.animate().alpha(0f)
                        email.animate().alpha(0f)
                        password.animate().alpha(0f)
                    }
                }

            })
        }
    }
}